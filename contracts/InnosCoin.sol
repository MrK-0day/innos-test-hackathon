pragma solidity ^0.4.24;

contract InnosCoin {
    uint256 private ClientCount;
    mapping (address => uint256) private balances;
    address public owner;
    
    constructor () public payable {
        require(msg.value == 1 ether, "1 Ether inital funding.");
        owner = msg.sender;
        ClientCount = 0;
    }
    
    function enroll () public returns (uint256) {
        balances[msg.sender] = 10 ether;
        return balances[msg.sender];
    }
    
    function deposit() public payable returns (uint256) {
        balances[msg.sender] += msg.value;
        return balances[msg.sender];
    }
    
    function withdraw(uint256 withdrawAmount) public returns (uint256) {
        if (withdrawAmount <= balances[msg.sender]) {
            balances[msg.sender] -= withdrawAmount;
            msg.sender.transfer(withdrawAmount);
        }
        return balances[msg.sender];
    }
    
    function balance() public view returns (uint256) {
        return balances[msg.sender];
    }
    
    function depositsBalance() public view returns (uint256) {
        return address(this).balance;
    }
}